package com.falabella.repository;

import com.falabella.entity.TrackingOrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Tabata Mackenzie Falabella
 */
@Repository
public interface TrackingOrderRepository extends JpaRepository<TrackingOrderEntity, Long> {

}
