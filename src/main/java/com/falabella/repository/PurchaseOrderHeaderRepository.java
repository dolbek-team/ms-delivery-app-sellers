package com.falabella.repository;

import com.falabella.entity.PurchaseOrderHeaderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PurchaseOrderHeaderRepository extends JpaRepository<PurchaseOrderHeaderEntity, Long> {


}
