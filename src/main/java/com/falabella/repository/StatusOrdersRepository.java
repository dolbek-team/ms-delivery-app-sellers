package com.falabella.repository;

import com.falabella.entity.StatusOrdersEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Tabata Mackenzie Falabella
 */
@Repository
public interface StatusOrdersRepository extends JpaRepository<StatusOrdersEntity, Long> {

}
