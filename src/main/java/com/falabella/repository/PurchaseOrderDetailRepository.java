package com.falabella.repository;

import com.falabella.entity.PurchaseOrderDetailEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PurchaseOrderDetailRepository extends JpaRepository<PurchaseOrderDetailEntity, Long> {

  List<PurchaseOrderDetailEntity> findByOrderHeaderId(Long orderHeaderId);

}
