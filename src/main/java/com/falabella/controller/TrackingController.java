package com.falabella.controller;

import com.falabella.service.TrackingOrderService;
import com.falabella.service.dto.TrackingOrderStatusDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author Tabata Mackenzie Falabella
 */
@Controller
public class TrackingController {

  private static final Logger LOG = LoggerFactory.getLogger(TrackingController.class);

  @Autowired
  TrackingOrderService trackingOrderService;


  @PostMapping(value = "/tracking", produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity<Void> imputeChangeStateTracking(
      @RequestBody TrackingOrderStatusDTO trackingOrder) throws Exception {

    LOG.debug("Rest POST: /tracking/status");

    trackingOrderService.imputeChangeStateTracking(trackingOrder);
    return ResponseEntity.status(HttpStatus.OK).build();
  }

}
