package com.falabella.controller;

import com.falabella.service.PurchaseOrderService;
import com.falabella.service.dto.PurchaseOrderDetailDTO;
import com.falabella.service.dto.PurchaseOrderHeaderDTO;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@ControllerAdvice
@RequestMapping("/api")
public class PurchaseOrderController {

  private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderController.class);

  @Autowired
  PurchaseOrderService purchaseOrderService;

  @GetMapping(value = "/purchase-order-header", produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity<List<PurchaseOrderHeaderDTO>> getPurchaseOrderHeader() {

    LOG.info("REST GET: /purchase-order-header");
    List<PurchaseOrderHeaderDTO> purchaseOrderHeaderList = purchaseOrderService
        .getPurchaseOrderHeader();

    return ResponseEntity.status(HttpStatus.OK).body(purchaseOrderHeaderList);
  }

  @GetMapping(value = "/purchase-order-detail/{idOrderDetail}", produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity<List<PurchaseOrderDetailDTO>> getPurchaseOrderDetail(
      @PathVariable(name = "idOrderDetail")Long idOrderDetail) throws Exception{

    LOG.info("REST GET: /purchase-order-detail/{}", idOrderDetail);
    List<PurchaseOrderDetailDTO> purchaseOrderDetailList = purchaseOrderService
        .getPurchaseOrderDetail(idOrderDetail);

    return ResponseEntity.status(HttpStatus.OK).body(purchaseOrderDetailList);
  }

}
