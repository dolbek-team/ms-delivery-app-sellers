package com.falabella.components.pubsub;

import com.google.api.core.ApiFuture;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.pubsub.v1.PubsubMessage;

/**
 * Author: Tabata Mackenzie. Sodimac.cl
 */

public interface ApiFutureService {

  ApiFuture<String> publishMessage(Publisher publisher, PubsubMessage pubsubMessage);
}
