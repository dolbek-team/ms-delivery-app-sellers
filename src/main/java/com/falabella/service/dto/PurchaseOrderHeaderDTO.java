package com.falabella.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PurchaseOrderHeaderDTO {

  private Long orderHeaderId;
  @JsonFormat(pattern = "yyyy-MM-dd")
  private LocalDate creationDate;
  @JsonFormat(pattern = "yyyy-MM-dd")
  private LocalDate deliveryDate;
  private Long status;
  private String timeRange;
  private String ide;
  private String customerName;
  private String address;
  private Long phoneNumber;
  private String latitude;
  private String longitude;

}
