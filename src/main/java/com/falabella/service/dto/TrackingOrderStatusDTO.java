package com.falabella.service.dto;

import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Tabata Mackenzie Sodimac S.A
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TrackingOrderStatusDTO {

  @NotBlank
  private Long order;
  @NotBlank
  private Long status;

}
