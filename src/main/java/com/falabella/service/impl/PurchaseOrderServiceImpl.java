package com.falabella.service.impl;

import com.falabella.entity.PurchaseOrderDetailEntity;
import com.falabella.entity.PurchaseOrderHeaderEntity;
import com.falabella.repository.PurchaseOrderDetailRepository;
import com.falabella.repository.PurchaseOrderHeaderRepository;
import com.falabella.service.PurchaseOrderService;
import com.falabella.service.dto.PurchaseOrderDetailDTO;
import com.falabella.service.dto.PurchaseOrderHeaderDTO;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PurchaseOrderServiceImpl implements PurchaseOrderService {

  private static final Logger LOG = LogManager.getLogger(PurchaseOrderServiceImpl.class);

  private static final String ENTITY_NOT_FOUND = "Entity not found";
  @Autowired
  PurchaseOrderHeaderRepository purchaseOrderHeaderRepository;
  @Autowired
  PurchaseOrderDetailRepository purchaseOrderDetailRepository;

  @Override
  public List<PurchaseOrderHeaderDTO> getPurchaseOrderHeader(){

    List<PurchaseOrderHeaderEntity> purchaseOrderHaederList = purchaseOrderHeaderRepository.findAll();

    List<PurchaseOrderHeaderDTO> purchaseOrderHeaderDTOList = purchaseOrderHaederList.stream()
        .map(this::convertPurchaseOrderHeaderEntityToDTO)
        .collect(Collectors.toList());

        return purchaseOrderHeaderDTOList;

  }

  @Override
  public List<PurchaseOrderDetailDTO> getPurchaseOrderDetail(Long idOrderDetail) throws Exception{


    List<PurchaseOrderDetailEntity> purchaseOrderDetailEntitiesList = purchaseOrderDetailRepository.findByOrderHeaderId(idOrderDetail);

    if(purchaseOrderDetailEntitiesList.isEmpty()){
      throw new Exception(ENTITY_NOT_FOUND);
    }

    List<PurchaseOrderDetailDTO> PurchaseOrderDetailDTO = purchaseOrderDetailEntitiesList.stream()
        .map(this::convertPurchaseDetailHeaderEntityToDTO)
        .collect(Collectors.toList());
    return PurchaseOrderDetailDTO;
  }


  private PurchaseOrderHeaderDTO convertPurchaseOrderHeaderEntityToDTO(
      PurchaseOrderHeaderEntity entity) {
    ModelMapper modelMapper = new ModelMapper();
    return modelMapper.map(entity, PurchaseOrderHeaderDTO.class);
  }


  private PurchaseOrderDetailDTO convertPurchaseDetailHeaderEntityToDTO(PurchaseOrderDetailEntity entity) {
    ModelMapper modelMapper = new ModelMapper();
    return modelMapper.map(entity, PurchaseOrderDetailDTO.class);
  }


}
