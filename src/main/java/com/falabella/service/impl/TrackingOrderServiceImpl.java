package com.falabella.service.impl;

import com.falabella.components.messages.Actions;
import com.falabella.components.messages.Message;
import com.falabella.components.pubsub.TopicPublisher;
import com.falabella.components.utils.Utils;
import com.falabella.config.GcpProperties;
import com.falabella.entity.PurchaseOrderHeaderEntity;
import com.falabella.entity.StatusOrdersEntity;
import com.falabella.repository.PurchaseOrderHeaderRepository;
import com.falabella.repository.StatusOrdersRepository;
import com.falabella.service.TrackingOrderService;
import com.falabella.service.dto.TrackingOrderStatusDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Tabata Mackenzie Sodimac S.A
 */
@Service
public class TrackingOrderServiceImpl implements TrackingOrderService {

  private static final Logger LOG = LogManager.getLogger(TrackingOrderServiceImpl.class);


  TopicPublisher topicPublisher;
  GcpProperties gcpProperties;
  StatusOrdersRepository statusOrdersRepository;
  PurchaseOrderHeaderRepository purchaseOrderHeaderRepository;
  Utils utils;

  @Autowired
  public TrackingOrderServiceImpl(TopicPublisher topicPublisher,
      GcpProperties gcpProperties,
      StatusOrdersRepository statusOrdersRepository,
      PurchaseOrderHeaderRepository purchaseOrderHeaderRepository,
      Utils utils) {
    this.topicPublisher = topicPublisher;
    this.gcpProperties = gcpProperties;
    this.statusOrdersRepository = statusOrdersRepository;
    this.purchaseOrderHeaderRepository = purchaseOrderHeaderRepository;
    this.utils = utils;
  }

  private final String CURRENT_NAME_CLASS = TrackingOrderServiceImpl.class.getSimpleName();
  private final String ENTITY_NOT_FOUND = "Entity Not Found";
  private final String ORDER_NOT_FOUND = "Order Not Found";


  /**
   * Impute change status Tracking 1 - Packing , 2 - On Route , 3 Delivered and 4 Pending
   */
  @Override
  public void imputeChangeStateTracking(TrackingOrderStatusDTO trackingOrderStatusDTO)
      throws Exception {

    LOG.info("Change Status Tracking : {}", trackingOrderStatusDTO);

    StatusOrdersEntity statusOrdersEntity = statusOrdersRepository
        .findById(trackingOrderStatusDTO.getStatus())
        .orElseThrow(() -> new Exception(ENTITY_NOT_FOUND));

    PurchaseOrderHeaderEntity purchaseOrderHeaderEntity = purchaseOrderHeaderRepository
        .findById(trackingOrderStatusDTO.getOrder())
        .orElseThrow(() -> new Exception(ORDER_NOT_FOUND));

    purchaseOrderHeaderEntity.setStatus(statusOrdersEntity.getId());

    purchaseOrderHeaderRepository.save(purchaseOrderHeaderEntity);

    Message message = utils
        .getPublishMessage(Actions.MODIFY.getValue(), CURRENT_NAME_CLASS,
            trackingOrderStatusDTO);
    topicPublisher.send(utils.convertToJson(message), gcpProperties.getTp_order_status());
  }
}
