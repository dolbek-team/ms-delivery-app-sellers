package com.falabella.service;

import com.falabella.service.dto.TrackingOrderStatusDTO;
import org.springframework.stereotype.Service;

/**
 * @author Tabata Mackenzie Sodimac S.A
 */
@Service
public interface TrackingOrderService {

  void imputeChangeStateTracking(TrackingOrderStatusDTO trackingOrderStatusDTO) throws Exception;

}
