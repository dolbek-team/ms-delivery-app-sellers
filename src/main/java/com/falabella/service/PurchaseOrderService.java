package com.falabella.service;

import com.falabella.service.dto.PurchaseOrderDetailDTO;
import com.falabella.service.dto.PurchaseOrderHeaderDTO;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public interface PurchaseOrderService {

  List<PurchaseOrderHeaderDTO> getPurchaseOrderHeader();
  List<PurchaseOrderDetailDTO> getPurchaseOrderDetail(Long idOrderDetail)throws Exception;

}
