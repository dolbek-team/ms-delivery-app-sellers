package com.falabella.entity;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.auto.value.AutoValue.Builder;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "purchase_header_order")
public class PurchaseOrderHeaderEntity {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "order_header_id")
  private Long orderHeaderId;

  @Column(name = "creation_date")
  @JsonFormat(pattern = "yyyy-MM-dd")
  private LocalDate creationDate;

  @Column(name = "delivery_date")
  @JsonFormat(pattern = "yyyy-MM-dd")
  private LocalDate deliveryDate;

  @Column(name = "status")
  private Long status;

  @Column(name = "time_range")
  private String timeRange;

  @Column(name = "ide")
  private String ide;

  @Column(name = "customer_name")
  private String customerName;

  @Column(name = "address")
  private String address;

  @Column(name = "phone_number")
  private Long phoneNumber;

  @Column(name = "latitude")
  private String latitude;

  @Column(name = "longitude")
  private String longitude;
}