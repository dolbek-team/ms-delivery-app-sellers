package com.falabella.entity;


import com.google.auto.value.AutoValue.Builder;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "purchase_detail_order")
public class PurchaseOrderDetailEntity {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "order_detail_id")
  private Long orderDetailId;

  @Column(name = "order_header_id", updatable = false)
  private Long orderHeaderId;

  @Column(name = "sku")
  private String sku;

  @Column(name = "sku_description")
  private String skuDescription;


}