package com.falabella.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Tabata Mackenzie Falabella
 */
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tracking_order")
public class TrackingOrderEntity {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "order_header_id")
  private Long orderHeaderId;

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "order_header_id", referencedColumnName = "order_header_id", updatable = false,
      insertable = false)
  private PurchaseOrderHeaderEntity purchaseOrderHeaderEntity;

  @Column(name = "status")
  private Long status;

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "id", referencedColumnName = "status", updatable = false,
      insertable = false)
  private StatusOrdersEntity statusOrdersEntity;

}
