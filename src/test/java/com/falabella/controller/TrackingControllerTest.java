package com.falabella.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.falabella.UtilTest;
import com.falabella.controller.utilsControllerTest.TestUtil;
import com.falabella.service.TrackingOrderService;
import com.falabella.service.dto.TrackingOrderStatusDTO;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * @author Tabata Mackenzie Falabella
 */
@RunWith(MockitoJUnitRunner.class)
public class TrackingControllerTest {

  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  @InjectMocks
  TrackingController trackingController;

  @Mock
  TrackingOrderService trackingOrderService;

  MockMvc mockMvc;

  @Before
  public void setUp() {
    mockMvc = MockMvcBuilders.standaloneSetup(trackingController).build();
  }

  @Test
  public void imputeChangeStateTracking_Should_Return_Ok() throws Exception {
    mockMvc.perform(post("/tracking").contentType(TestUtil.APPLICATION_JSON_UTF8)
        .content(UtilTest.convertToJson(trackingOrderStatusMock()))).andExpect(status().isOk())
        .andDo(
            MockMvcResultHandlers.print());
  }

  private TrackingOrderStatusDTO trackingOrderStatusMock() {
    return TrackingOrderStatusDTO.builder().order(203090L).status(1L).build();
  }

}
