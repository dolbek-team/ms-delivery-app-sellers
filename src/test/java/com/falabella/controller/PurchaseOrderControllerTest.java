package com.falabella.controller;


import com.falabella.service.PurchaseOrderService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(MockitoJUnitRunner.class)
@ActiveProfiles({"test"})
public class PurchaseOrderControllerTest {

  @InjectMocks
  private PurchaseOrderController purchaseOrderController;

  private MockMvc mockMvc;

  @Mock
  private PurchaseOrderService purchaseOrderService;

  @Before
  public void setUp() {
    mockMvc = MockMvcBuilders.standaloneSetup(purchaseOrderController).build();
  }


  @Test
  public void getPurchaseOrderHeader_ShouldBe_OK() throws Exception {
    MockHttpServletRequestBuilder builder =
        MockMvcRequestBuilders.get("/api/purchase-order-header")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .accept(MediaType.APPLICATION_JSON);

    this.mockMvc.perform(builder).andExpect(MockMvcResultMatchers.status().isOk())
        .andDo(MockMvcResultHandlers.print());
  }

  @Test
  public void getPurchaseOrderDetail_ShouldBe_OK() throws Exception {
    MockHttpServletRequestBuilder builder =
        MockMvcRequestBuilders.get("/api/purchase-order-detail/10")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .accept(MediaType.APPLICATION_JSON);

    this.mockMvc.perform(builder).andExpect(MockMvcResultMatchers.status().isOk())
        .andDo(MockMvcResultHandlers.print());
  }

}
