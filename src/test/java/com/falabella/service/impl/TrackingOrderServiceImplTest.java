package com.falabella.service.impl;


import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.falabella.components.pubsub.TopicPublisher;
import com.falabella.components.utils.Utils;
import com.falabella.config.GcpProperties;
import com.falabella.entity.PurchaseOrderHeaderEntity;
import com.falabella.entity.StatusOrdersEntity;
import com.falabella.repository.PurchaseOrderHeaderRepository;
import com.falabella.repository.StatusOrdersRepository;
import com.falabella.service.dto.TrackingOrderStatusDTO;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * @author Tabata Mackenzie Falabella S.A
 */
@RunWith(MockitoJUnitRunner.class)
public class TrackingOrderServiceImplTest {

  @Mock
  TopicPublisher topicPublisher;

  @Mock
  GcpProperties gcpProperties;

  @Mock
  StatusOrdersRepository statusOrdersRepository;

  @Mock
  PurchaseOrderHeaderRepository purchaseOrderHeaderRepository;

  @Mock
  Utils utils;

  @InjectMocks
  TrackingOrderServiceImpl trackingOrderService;

  private final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

  @Test
  public void imputeChangeStateTracking() throws Exception {

    Mockito.when(statusOrdersRepository.findById(Mockito.anyLong())).thenReturn(getStatusOrder(1L));
    Mockito.when(purchaseOrderHeaderRepository.findById(Mockito.anyLong()))
        .thenReturn(Optional.of(getPurchaseHeaderMock()));
    trackingOrderService.imputeChangeStateTracking(trackingOrderStatusMock());
    verify(purchaseOrderHeaderRepository, times(1)).save(any());
  }

  private Optional<StatusOrdersEntity> getStatusOrder(Long order) {
    List<StatusOrdersEntity> getAllStatusOrder = new ArrayList<>();
    getAllStatusOrder.add(StatusOrdersEntity.builder().id(1L).description("Packing").build());
    getAllStatusOrder.add(StatusOrdersEntity.builder().id(2L).description("On Route").build());
    getAllStatusOrder.add(StatusOrdersEntity.builder().id(3L).description("Delivered").build());

    return getAllStatusOrder.stream().parallel()
        .filter(statusOrdersEntity -> statusOrdersEntity.getId().equals(order))
        .findAny();
  }


  private PurchaseOrderHeaderEntity getPurchaseHeaderMock() {
    PurchaseOrderHeaderEntity purchaseOrderHeaderEntity = new PurchaseOrderHeaderEntity();
    purchaseOrderHeaderEntity.setStatus(2L);
    purchaseOrderHeaderEntity.setOrderHeaderId(1000900L);
    purchaseOrderHeaderEntity.setDeliveryDate(LocalDate.parse("2020-07-28", formatter));
    purchaseOrderHeaderEntity.setCreationDate(LocalDate.parse("2020-07-28", formatter));
    return purchaseOrderHeaderEntity;
  }

  private TrackingOrderStatusDTO trackingOrderStatusMock() {
    return TrackingOrderStatusDTO.builder().order(1000900L).status(3L).build();
  }
}