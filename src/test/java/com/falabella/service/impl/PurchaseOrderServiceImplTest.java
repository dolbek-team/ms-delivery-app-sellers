package com.falabella.service.impl;

import com.falabella.entity.PurchaseOrderDetailEntity;
import com.falabella.entity.PurchaseOrderHeaderEntity;
import com.falabella.repository.PurchaseOrderDetailRepository;
import com.falabella.repository.PurchaseOrderHeaderRepository;
import com.falabella.service.PurchaseOrderService;
import com.falabella.service.dto.PurchaseOrderDetailDTO;
import com.falabella.service.dto.PurchaseOrderHeaderDTO;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class PurchaseOrderServiceImplTest {

  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  @Autowired
  PurchaseOrderService purchaseOrderService;

  @MockBean
  PurchaseOrderHeaderRepository purchaseOrderHeaderRepository;
  @MockBean
  PurchaseOrderDetailRepository purchaseOrderDetailRepository;


  @TestConfiguration
  static class EdgeNodeServiceImplContextConfiguration {

    @Bean
    public PurchaseOrderService purchaseOrderService() {
      return new PurchaseOrderServiceImpl();
    }
  }


  @Test
  public void getPurchaseOrderHeader_shouldBeNotNullAndNotEmpty(){

    List<PurchaseOrderHeaderEntity> purchaseOrderHeaderEntityList = purchaseOrderHeaderMock();


    Mockito.when(purchaseOrderHeaderRepository.findAll())
        .thenReturn(purchaseOrderHeaderEntityList);


    List<PurchaseOrderHeaderDTO> purchaseOrderHeaderDTOList = purchaseOrderService
        .getPurchaseOrderHeader();

    Assert.assertNotNull(purchaseOrderHeaderDTOList);
  }


  @Test
  public void getPurchaseOrderDetail_shouldBeNotNullAndNotEmpty() throws Exception{

    List<PurchaseOrderDetailEntity> purchaseOrderDetailEntityList = purchaseOrderDetailMock();


    Mockito.when(purchaseOrderDetailRepository.findByOrderHeaderId(Mockito.anyLong()))
        .thenReturn(purchaseOrderDetailEntityList);


    List<PurchaseOrderDetailDTO> purchaseOrderDetailDTOList = purchaseOrderService
        .getPurchaseOrderDetail(135678491L);

    Assert.assertNotNull(purchaseOrderDetailDTOList);
  }

  @Test(expected = Exception.class)
  public void getPurchaseOrderDetail_ReturnExceptionWhenIdOrderNotExist() throws Exception{


    Mockito.when(purchaseOrderDetailRepository.findByOrderHeaderId(Mockito.anyLong()))
        .thenReturn(null);


    List<PurchaseOrderDetailDTO> purchaseOrderDetailDTOList = purchaseOrderService
        .getPurchaseOrderDetail(null);
  }

  public List<PurchaseOrderHeaderEntity> purchaseOrderHeaderMock(){
    PurchaseOrderHeaderEntity purchaseOrderHeaderEntity = new PurchaseOrderHeaderEntity();
    List<PurchaseOrderHeaderEntity> purchaseOrderHeaderEntityList = new ArrayList<>();

    purchaseOrderHeaderEntity.setOrderHeaderId(135678491L);

    purchaseOrderHeaderEntityList.add(purchaseOrderHeaderEntity);

    return purchaseOrderHeaderEntityList;

  }

  public List<PurchaseOrderDetailEntity> purchaseOrderDetailMock(){
    PurchaseOrderDetailEntity purchaseOrderDetailEntity = new PurchaseOrderDetailEntity();
    List<PurchaseOrderDetailEntity> purchaseOrderDetailEntityList = new ArrayList<>();


    purchaseOrderDetailEntity.setOrderDetailId(1L);
    purchaseOrderDetailEntity.setOrderHeaderId(135678491L);

    purchaseOrderDetailEntityList.add(purchaseOrderDetailEntity);

    return purchaseOrderDetailEntityList;

  }


}
