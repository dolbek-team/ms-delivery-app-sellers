# Delivery App Sellers 
 Delivery app sellers for the Hackaton Falabella -  Hack to the Future

 ## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
Da un ejemplo
```

### Instalación 🔧

_Una serie de ejemplos paso a paso que te dice lo que debes ejecutar para tener un entorno de desarrollo ejecutandose_

_Dí cómo será ese paso_


## Despliegue 📦

_Agrega notas adicionales sobre como hacer deploy_

## Construido con 🛠️

* [Ionic Version 4](http://www.dropwizard.io/1.0.2/docs/) - Framework for developing hybrid mobile
* [Angular 8](https://angular.io/) - Angular is a framework for web applications developed in TypeScript
* [Springboot](https://spring.io/) - Springboot is a framework for developed microservices

 

## Contribuyendo 🖇️

Por favor lee el [CONTRIBUTING.md](https://gitlab.com/dolbek-team/ms-delivery-app-sellers) para detalles de nuestro código de conducta, y el proceso para enviarnos pull requests.

## Autors ✒️

* **Sebastian Arancibia** - *Full Stack Developer* - [searancibiac](searancibiac@falabella.cl)
* **Tabata Mackenzie** - *Backend Developer* - [tmackenzie](#tmackenzie@falabella.cl)
* **Nicolas Provoste** - *Backend Developer* - [nprovoste](#nprovosteb@falabella.cl)
* **Andres Garcia** - *Backend Developer* - [ext_andgarciaa](#ext_andgarciaa@Sodimac.cl)
* **Jesus Fernandez** - *Scrum Master* - [jlfernandezu](#jlfernandezu@falabella.cl)
